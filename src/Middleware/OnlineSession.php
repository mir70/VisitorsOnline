<?php

namespace mirovich\VisitorsOnline\Middleware;

use Closure;
use mirovich\VisitorsOnline\Models\VisitorOnline;

class OnlineSession
{
    public function handle($request, Closure $next)
    {
        VisitorOnline::updateOrCreate([
                'session' => session()->getId(),
        ],[
                'page' => request()->path(),
                'user_id' => (auth()->check() ? auth()->id() : null),
                'updated_at' => now(),
           ]
        );

        return $next($request);
    }
}
