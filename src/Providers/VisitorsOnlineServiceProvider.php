<?php

namespace mirovich\VisitorsOnline\Providers;

use Illuminate\Support\ServiceProvider;
use mirovich\VisitorsOnline\Middleware\OnlineSession;
use mirovich\VisitorsOnline\Models\VisitorOnline;

class VisitorsOnlineServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->publishes([
            __DIR__.'/../config/visitors.php' => config_path('visitors.php'),
        ]);
        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('web', OnlineSession::class);
        $this->app->bind('VisitorsOnline', function() {
            $visitor = new VisitorOnline();
            return new VisitorOnline();
        });
    }
}
