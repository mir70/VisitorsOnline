<?php

namespace mirovich\VisitorsOnline\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class VisitorOnline extends Model
{
    use HasFactory;

    protected $table = 'visitors_online';

    protected $fillable = ['session', 'user_id', 'page', 'updated_at'];

    /**
     * @return int
     */
    public static function getOnlineVisitorsCount()
    {
        return self::where("updated_at", ">", now()->subMinutes(config("visitor.online_timer")))
            ->count();
    }

    /**
     * @param $page string
     * @return int
     */
    public static function getOnlineVisitorsOnPageCount($page)
    {
        return self::where("updated_at", ">", now()->subMinutes(config("visitor.online_timer")))
            ->where("page", $page)
            ->count();
    }

    /**
     * @return int
     */
    public static function getOnlineUsersCount()
    {
        return self::where("updated_at", ">", now()->subMinutes(config("visitor.online_timer")))
            ->whereNotNull("user_id")
            ->count();
    }

    /**
     * @param $page string
     * @return int
     */
    public static function getOnlineUsersOnPageCount($page)
    {
        return self::where("updated_at", ">", now()->subMinutes(config("visitor.online_timer")))
            ->whereNotNull("user_id")
            ->where("page",$page)
            ->count();
    }

    /**
     * @return $this
     */
    public static function getOnlineUsersList()
    {
        return self::where("updated_at", ">", now()->subMinutes(config("visitor.online_timer")))
            ->whereNotNull("user_id")
            ->with("user")
            ->get();
    }

    /**
     * @param string
     * @return $this
     */
    public static function getOnlineUsersOnPageList($page)
    {
        return self::where("updated_at", ">", now()->subMinutes(config("visitor.online_timer")))
            ->whereNotNull("user_id")
            ->where("page",$page)
            ->with("user")
            ->get();
    }

    public static function clearOld()
    {
        return self::where("updated_at", "<", now()->addMinutes(config('visitor.clear_timer')));
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
