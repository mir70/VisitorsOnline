<?php

namespace mirovich\VisitorsOnline\Facades;

use Illuminate\Support\Facades\Facade;
use mirovich\VisitorsOnline\Models\VisitorOnline;

/**
 * @method static int getOnlineVisitorsCount
 * @method static int getOnlineVisitorsOnPageCount($page)
 * @method static int getOnlineUsersCount
 * @method static int getOnlineUsersOnPageCount($page)
 * @method static VisitorOnline getOnlineUsersList
 * @method static VisitorOnline getOnlineUsersOnPageList($page)
 * @method static boolean clearOld
 */
class VisitorsOnline extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'VisitorsOnline';
    }
}
