<?php

return [

    'online_timer' => env('ONLINE_TIMER', 5),

    'clear_timer'  => env('CLEAR_TIMER', 60),
];
