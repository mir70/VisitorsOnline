# VisitorsOnline
shows visitors online Laravel 8.*

# Install

`composer require mirovich/visitorsonline`

`php artisan migrate`

# Usage
```
use mirovich\VisitorsOnline\Facades\VisitorsOnline;

VisitorsOnline::getOnlineVisitorsCount(); 

VisitorsOnline::clearOld(); //removing old records
```
